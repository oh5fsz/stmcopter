#include <stm32f1xx.h>
#include "FreeRTOS.h"
#include "task.h"
#include "quad.h"

void vApplicationMallocFailedHook(void)
{
    for (;;) {}
}

void HardFault_Handler(void)
{
    for (;;) {}
}

static void setup_clocks()
{
    /* Enable HSE */
    RCC->CR |= RCC_CR_HSEON;
    /* Wait for HSERDY */
    while (!(RCC->CR & RCC_CR_HSERDY));
    /* Flash latency must be set to 2 */
    FLASH->ACR |= (2 << FLASH_ACR_LATENCY_Pos);
    while ((FLASH->ACR & FLASH_ACR_LATENCY) != 2);
    /* Make sure PLL is disabled */
    RCC->CR &= ~RCC_CR_PLLON;
    /* Set PLL multiplier to 9 */
    RCC->CFGR |= (7 << RCC_CFGR_PLLMULL_Pos);
    /* Set PLLSRC to HSE */
    RCC->CFGR |= RCC_CFGR_PLLSRC;
    /* Divide APB1 frequency by 2 */
    RCC->CFGR |= (4 << RCC_CFGR_PPRE1_Pos);
    /* Enable PLL */
    RCC->CR |= RCC_CR_PLLON;
    /* Wait for PLL to be ready */
    while (!(RCC->CR & RCC_CR_PLLRDY));
    /* Set system clock source to PLL */
    RCC->CFGR |= (2 << RCC_CFGR_SW_Pos);
}

int main(void)
{
    /* Throw out JTAG */
    RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;
    AFIO->MAPR |= (2 << AFIO_MAPR_SWJ_CFG_Pos);
    /* Set up system clocks */
    setup_clocks();
    /* Update SystemCoreClock value */
    SystemCoreClockUpdate();
    /* Initialize interrupts priorities for FreeRTOS */
    NVIC_SetPriorityGrouping((uint32_t)0x00000003);
	NVIC_SetPriority(PendSV_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),15, 0));
	NVIC_SetPriority(SysTick_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),15, 0));
    /* Initialize and start SysTick */
    SysTick->LOAD = (uint32_t)(SystemCoreClock / 1000)-1UL;
    SysTick->VAL = 0UL;
    SysTick->CTRL = SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk;
    /* Initialize the system and start the FreeRTOS scheduler */
    quad_startup();
    vTaskStartScheduler();
    /* We should not ever get here */
    for (;;) {}
}

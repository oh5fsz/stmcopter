#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "quad.h"
#include "FreeRTOS.h"
#include "task.h"
#include <usart/usart.h>
#include <pwm/pwm.h>
#include <i2c/i2c.h>
#include "microshell.h"
#include "mscmd.h"
#include "printf.h"

extern struct pwm motors[4];

char input_buffer[128];
static struct usart *debug_port = NULL;
extern struct i2c i2c_dev;

static MSCMD mscmd;
static MICROSHELL ms;

void _putchar(char c)
{
    usart_write(debug_port, &c, 1);
}

static MSCMD_USER_RESULT quad_motor_cmd(MSOPT *msopt, MSCMD_USER_OBJECT usrobj)
{
    int idx;
    uint32_t width_us;
    int argc;
    char txbuf[128];
    char argv[MSCONF_MAX_INPUT_LENGTH];

    msopt_get_argc(msopt, &argc);
    if (argc == 1) {
        snprintf(txbuf, 128, "M1: %u\r\nM2: %u\r\nM3: %u\r\nM4: %u\r\n",
            pwm_get_width_us(&motors[0]),
            pwm_get_width_us(&motors[1]),
            pwm_get_width_us(&motors[2]),
            pwm_get_width_us(&motors[3]));
        usart_write(debug_port, txbuf, strlen(txbuf));
    } else if (argc == 3) {
        msopt_get_argv(msopt, 1, argv, MSCONF_MAX_INPUT_LENGTH);
        idx = atoi(argv);
        if (idx >= 0 && idx <= 4) {
            msopt_get_argv(msopt, 2, argv, MSCONF_MAX_INPUT_LENGTH);
            width_us = atoi(argv);
            snprintf(txbuf, 128, "Set motor %u to %u\r\n", idx, width_us);
            usart_write(debug_port, txbuf, strlen(txbuf));
            pwm_set_width_us(&motors[idx], width_us);
        }
    } else if (argc == 2) {
        msopt_get_argv(msopt, 1, argv, MSCONF_MAX_INPUT_LENGTH);
        if (strcmp(argv, "calib") == 0) {
            for (idx = 0; idx < 4; idx++) {
                pwm_set_width_us(&motors[idx], 2000);
            }
            usart_write(debug_port, "Connect power now\r\n", 19);
            vTaskDelay(5000);
            for (idx = 0; idx < 4; idx++) {
                pwm_set_width_us(&motors[idx], 1000);
            }
        }
        usart_write(debug_port, "Calibration done!\r\n", 19);
    }
    return 0;
}

static MSCMD_USER_RESULT quad_stats_cmd(MSOPT *msopt, MSCMD_USER_OBJECT usrobj)
{
    char buf[16];
    uint32_t tx_count = debug_port->tx_count;
    uint32_t rx_count = debug_port->rx_count;

    usart_write(debug_port, "TXCnt: ", 7);
    snprintf(buf, 16, "%u\r\n", tx_count);
    usart_write(debug_port, buf, strlen(buf));
    usart_write(debug_port, "RXCnt: ", 7);
    snprintf(buf, 16, "%u\r\n", rx_count);
    usart_write(debug_port, buf, strlen(buf));
    return 0;
}

static MSCMD_USER_RESULT quad_i2c_cmd(MSOPT *msopt, MSCMD_USER_OBJECT usrobj)
{
    char argv[MSCONF_MAX_INPUT_LENGTH];
    uint8_t buf[16];
    int argc;
    int32_t reg;
    int32_t addr;
    int32_t len;
    int32_t i;

    msopt_get_argc(msopt, &argc);
    if (argc < 5 || argc > 5) {
        printf("Usage: i2c read/write addr reg len\r\n");
        return -1;
    }
    msopt_get_argv(msopt, 1, argv, MSCONF_MAX_INPUT_LENGTH);
    if (strncmp(argv, "read", 4) == 0) {
        msopt_get_argv(msopt, 2, argv, MSCONF_MAX_INPUT_LENGTH);
        addr = atoi(argv);
        msopt_get_argv(msopt, 3, argv, MSCONF_MAX_INPUT_LENGTH);
        reg = atoi(argv);
        msopt_get_argv(msopt, 4, argv, MSCONF_MAX_INPUT_LENGTH);
        len = atoi(argv);
        if (len > 16) {
            printf("Read max 16 bytes\r\n");
            return -1;
        }
        i2c_read_reg(&i2c_dev, addr, reg, buf, len);
        printf("Read %d bytes from addr %d and reg %d\r\n", len, addr, reg);
        printf("Response: ");
        for (i = 0; i < len; i++) {
            printf("0x%x ", buf[i]);
        }
        printf("\r\n");
    } else {
        printf("Write test\n");
        uint8_t reg = 0b00000001;
        i2c_write_reg(&i2c_dev, 104, 107, &reg, 1);
    }
    return 0;
}

static MSCMD_COMMAND_TABLE cmdtable[] = {
    { "motor", quad_motor_cmd },
    { "stats", quad_stats_cmd },
    { "i2c", quad_i2c_cmd }
};

static char ms_read(void)
{
    char c;
    
    usart_read(debug_port, &c, 1);
    return c;
}

static void ms_write(char c)
{
    usart_write(debug_port, &c, 1);
}

void quad_cli_task(void *args)
{
    MSCMD_USER_RESULT r;
    debug_port = (struct usart *)args;
    microshell_init(&ms, &ms_write, &ms_read, 0);
    mscmd_init(&mscmd, cmdtable, sizeof(cmdtable)/sizeof(cmdtable[0]), debug_port);

    for (;;) {
        usart_write(debug_port, "\r\n", 2);
        usart_write(debug_port, "stmcopter> ", 11);
        microshell_getline(&ms, input_buffer, 128);
        mscmd_execute(&mscmd, input_buffer, &r);
    }
}
#include "stm32f1xx.h"
#include <pwm/pwm.h>
#include <usart/usart.h>
#include <i2c/i2c.h>
#include <receiver/receiver.h>
#include "quad.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

TaskHandle_t quad_cli_task_handle;
TaskHandle_t quad_dbg_task_handle;

struct pwm motors[4];
struct i2c i2c_dev;
struct usart debug_port;
struct receiver rcvr;

void quad_startup()
{
    pwm_init(&motors[0], FRONT_LEFT, NULL);
    pwm_init(&motors[1], FRONT_RIGHT, NULL);
    pwm_init(&motors[2], REAR_LEFT, NULL);
    pwm_init(&motors[3], REAR_RIGHT, NULL);
    usart_init(&debug_port, (void*)USART2);
    i2c_init(&i2c_dev, (void*)I2C1);
    receiver_init(&rcvr, NULL);
    xTaskCreate(quad_cli_task, "cli", 256, (void*)&debug_port, 2, &quad_cli_task_handle);
}

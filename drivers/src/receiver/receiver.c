#include "FreeRTOS.h"
#include "semphr.h"
#include "receiver/receiver.h"

extern struct receiver_api stm32_ppm_api;

int receiver_init(struct receiver *rcvr, void *ctx)
{
    uint32_t i;
    /* Initialize channels to 0 */
    for (i = 0; i < MAX_CHANNELS; i++) {
        rcvr->channels[i] = 0;
    }
    rcvr->api = &stm32_ppm_api;
    rcvr->mutex = xSemaphoreCreateMutex();
    rcvr->api->init(rcvr, ctx);
    return 0;
}

int receiver_get_channels(struct receiver *rcvr, uint32_t *val)
{
    uint32_t i;
    int ret = -1;

    if (xSemaphoreTake(rcvr->mutex, 10) == pdTRUE) {
        /* Copy channel values to destination array */
        for (i = 0; i < MAX_CHANNELS; i++) {
            val[i] = rcvr->channels[i];
        }
        xSemaphoreGive(rcvr->mutex);
        ret = 0;
    }
    return ret;
}
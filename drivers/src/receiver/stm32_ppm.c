#include "stm32f1xx.h"
#include "receiver/receiver.h"

static uint32_t eof = 0;
static uint32_t cur_ch;
static volatile uint32_t micros;
static volatile uint32_t last;
static volatile uint32_t pulse;
static struct receiver *__receiver;

static int stm32_ppm_init(struct receiver *rcvr, void *ctx)
{
    __receiver = rcvr;
    micros = 0;
    last = 0;
    pulse = 0;
    /* Make sure clock is enabled for GPIOB */
    RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;
    /* Enable clock for TIM2 */
    RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
    /* Set PB9 as input pull-down */
    GPIOB->CRL &= ~((3 << GPIO_CRL_CNF4_Pos) | (3 << GPIO_CRL_MODE4_Pos));
    GPIOB->CRL |= (2 << GPIO_CRL_CNF4_Pos);
    GPIOB->ODR |= GPIO_ODR_ODR4;
    /* Set prescaler to 35 (1MHz clock) */
    TIM3->PSC = 71;
    /* Set up TIM3_CH1 as input and enable counter */
    TIM3->ARR = 0xFFFFFFFF;
    TIM3->CR1 |= TIM_CR1_CEN | TIM_CR1_ARPE;
    AFIO->EXTICR[1] |= (1 << AFIO_EXTICR2_EXTI4_Pos);
    EXTI->IMR |= EXTI_IMR_IM4;
    EXTI->RTSR |= EXTI_RTSR_TR4;
    NVIC_SetPriority(EXTI4_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 10, 0));
    NVIC_EnableIRQ(EXTI4_IRQn);
    return 0;
}

void EXTI4_IRQHandler()
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    uint32_t micros = TIM3->CNT;
    pulse = micros - last;
    if (EXTI->PR & EXTI_PR_PR4) {
        EXTI->PR |= EXTI_PR_PR4;
    }
    /* Wait for PPM start pulse */
    if (eof == 0 && pulse > 5000) {
        eof = 1;
    } else if (eof == 1) {
        /* Update channel */
        if (xSemaphoreTakeFromISR(__receiver->mutex, &xHigherPriorityTaskWoken) == pdTRUE) {
            /* Avoid glitching when using debugger */
            if (pulse < 2500) {
                __receiver->channels[cur_ch] = pulse;
            }
            cur_ch++;
            if (cur_ch > MAX_CHANNELS - 1) {
                cur_ch = 0;
                eof = 0;
            }
            xSemaphoreGiveFromISR(__receiver->mutex, &xHigherPriorityTaskWoken);
        }
    }
    last = micros;
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

struct receiver_api stm32_ppm_api = {
    .init = stm32_ppm_init
};
#include "stm32f1xx.h"
#include "i2c/i2c.h"


typedef enum
{
    I2C_START,
    I2C_REPEAT,
    I2C_WRITE_REG,
    I2C_READ_DATA,
    I2C_WRITE_DATA,
    I2C_STOP
} i2c_state_t;

struct i2c_transaction
{
    uint8_t addr;
    uint8_t btfcnt;
    uint8_t reg;
    uint8_t *data;
    uint8_t dir;
    uint32_t len;
    uint32_t cnt;
    i2c_res_t res;
    i2c_state_t state;
    TaskHandle_t user;
};

static struct i2c_transaction __trans;

static void stm32_i2c_init(struct i2c *dev, void *ctx)
{
    /* Make sure clock for GPIOB is enabled */
    RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;
    /* Enable clock for DMA */
    RCC->AHBENR |= RCC_AHBENR_DMA1EN;

    /* Configure DMA TX channel */
    DMA1_Channel7->CCR |= (2 << DMA_CCR_PL_Pos)
                        | DMA_CCR_MINC
                        | DMA_CCR_DIR;
    DMA1_Channel7->CPAR = (uint32_t)&I2C1->DR;

    /* Configure DMA RX channel */
    DMA1_Channel6->CCR |= (2 << DMA_CCR_PL_Pos)
                        | DMA_CCR_MINC;
    DMA1_Channel6->CPAR = (uint32_t)&I2C1->DR;
    /* Set PB6, PB7 as high-speed outputs */
    GPIOB->CRL |= (3 << GPIO_CRL_MODE6_Pos) | (3 << GPIO_CRL_MODE7_Pos);
    /* Set PB6, PB7 as alternate function (open drain) */
    GPIOB->CRL |= (3 << GPIO_CRL_CNF6_Pos) | (3 << GPIO_CRL_CNF7_Pos);
    /* Enable clock for I2C1 */
    RCC->APB1ENR |= RCC_APB1ENR_I2C1EN;
    /* Reset peripheral */
    I2C1->CR1 |= I2C_CR1_SWRST;
    I2C1->CR1 &= ~I2C_CR1_SWRST;
    /* APB1 speed is system clock / 2 */
    I2C1->CR2 |= (36 << I2C_CR2_FREQ_Pos);
    /* Set timings for 100kHz */
    I2C1->CCR = 180 << I2C_CCR_CCR_Pos;
    /* MPU-6050 max rise time is 300ns */
    I2C1->TRISE = 11 << I2C_TRISE_TRISE_Pos;
    /* Enable I2C and hope for the best */
    I2C1->CR1 |= I2C_CR1_PE;
    I2C1->CR2 |= I2C_CR2_ITBUFEN | I2C_CR2_ITEVTEN;
    NVIC_SetPriority(I2C1_ER_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 10, 0));
    NVIC_SetPriority(I2C1_EV_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 11, 0));
    NVIC_EnableIRQ(I2C1_ER_IRQn);
    NVIC_EnableIRQ(I2C1_EV_IRQn);
}


static i2c_res_t stm32_i2c_read_reg(struct i2c *dev, uint8_t addr, uint8_t reg, uint8_t *data, int32_t len)
{
    __trans.user = xTaskGetCurrentTaskHandle();
    __trans.addr = (addr << 1);
    __trans.btfcnt = 0;
    __trans.reg = reg;
    __trans.data = data;
    __trans.len = len;
    __trans.dir = 0;
    __trans.cnt = 0;
    __trans.state = I2C_START;
    
    /* Clear flags */
    I2C1->SR1 = 0x00;
    /* Generate START condition and start the transfer */
    I2C1->CR1 |= I2C_CR1_START | I2C_CR1_ACK;
    /* Wait for transaction to finish */
    ulTaskNotifyTake(pdTRUE, 100);
    return __trans.res;
}

static i2c_res_t stm32_i2c_write_reg(struct i2c *dev, uint8_t addr, uint8_t reg, uint8_t *data, int32_t len)
{
    __trans.user = xTaskGetCurrentTaskHandle();
    __trans.addr = (addr << 1);
    __trans.btfcnt = 0;
    __trans.reg = reg;
    __trans.data = data;
    __trans.len = len;
    __trans.dir = 1;
    __trans.cnt = 0;
    __trans.state = I2C_START;

    /* Clear SR */
    I2C1->SR1 = 0x00;
    /* Generate START condition and start the transfer */
    I2C1->CR1 |= I2C_CR1_START | I2C_CR1_ACK;
    /* Wait for transaction to finish */
    ulTaskNotifyTake(pdTRUE, 100);
    return __trans.res;
}

static void __stm32_irq_sb_handler()
{
    /*
        If we are reading data and
        we sent the register number already
        this is a repeated start.
    */
    if (__trans.state == I2C_REPEAT) {
        I2C1->DR = (__trans.addr | 0x01);
        /* Handle special case */
        if (__trans.len == 2) {
            I2C1->CR1 |= I2C_CR1_POS;
        }
        __trans.state = I2C_READ_DATA;
    /*
        Else send with RW bit as 0 as we are writing
        or writing the register to read.
    */
    } else {
        I2C1->DR = __trans.addr;
    }
}

static void __stm32_irq_addr_handler()
{
    uint8_t sr2;
    if (__trans.state == I2C_START) {
        sr2 = I2C1->SR2;
        I2C1->DR = __trans.reg;
        __trans.state = I2C_WRITE_REG;
    } else {
        /* Handle special cases */
        if (__trans.len == 1 && __trans.dir == 0) {
            I2C1->CR1 &= ~I2C_CR1_ACK;
            sr2 = I2C1->SR2;
            I2C1->CR1 |= I2C_CR1_STOP;
            __trans.state = I2C_READ_DATA;
        } else if (__trans.len == 2 && __trans.dir == 0) {
            sr2 = I2C1->SR2;
            I2C1->CR1 &= ~I2C_CR1_ACK;
            __trans.state = I2C_READ_DATA;
        } else {
            sr2 = I2C1->SR2;
            if (__trans.dir == 0) {
                __trans.state = I2C_READ_DATA;
            } else {
                __trans.state = I2C_WRITE_DATA;
            }
        }
    }
}

static void __stm32_irq_txe_handler()
{
    if (__trans.state == I2C_WRITE_REG) {
        if (__trans.dir == 0) {
            /* Register address was written */
            /* Send repeated START */
            I2C1->CR1 |= I2C_CR1_START;
            __trans.state = I2C_REPEAT;
        } else {
            __trans.state = I2C_WRITE_DATA;
        }
    } else if (__trans.state == I2C_WRITE_DATA) {
        /* If data written - send a STOP */
        if (__trans.cnt == __trans.len) {
            I2C1->CR1 |= I2C_CR1_STOP;
        } else {
            /* Otherwise write first data byte */
            I2C1->DR = *__trans.data++;
            __trans.cnt++;
        }
    }
}

static void __stm32_irq_rxne_handler()
{
    if (__trans.state == I2C_READ_DATA) {
        if (__trans.len != 2) {
            *__trans.data++ = I2C1->DR;
            __trans.cnt++;
        }
        if (__trans.len > 2 && __trans.cnt == __trans.len - 1) {
            /* Clear ACK (NACK sent on next RX) */
            I2C1->CR1 &= ~I2C_CR1_ACK;
            I2C1->CR1 |= I2C_CR1_STOP;
        }
    }
}

static void __stm32_irq_btf_handler()
{
    /* Handle special case when len == 2 */
    if (__trans.state == I2C_READ_DATA && __trans.len == 2) {
        /* Mask interrupts (check errata) */
        I2C1->CR1 |= I2C_CR1_STOP;
        *__trans.data++ = I2C1->DR;
        __disable_irq();
        *__trans.data++ = I2C1->DR;
        __enable_irq();
        __trans.cnt += 2;
    }
}

void I2C1_EV_IRQHandler()
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    if (I2C1->SR1 & I2C_SR1_SB) {
        __stm32_irq_sb_handler();
    }
    if (I2C1->SR1 & I2C_SR1_ADDR) {
        __stm32_irq_addr_handler();
    }
    if (I2C1->SR1 & I2C_SR1_TXE) {
        __stm32_irq_txe_handler();
    }
    if (I2C1->SR1 & I2C_SR1_BTF) {
        __stm32_irq_btf_handler();
    }
    if (I2C1->SR1 & I2C_SR1_RXNE) {
        __stm32_irq_rxne_handler();
    }
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

void I2C1_ER_IRQHandler()
{

}

struct i2c_api stm32_i2c_api = {
    .init = stm32_i2c_init,
    .read_reg = stm32_i2c_read_reg,
    .write_reg = stm32_i2c_write_reg
};
#include "i2c/i2c.h"
#include "FreeRTOS.h"
#include "semphr.h"

extern struct i2c_api stm32_i2c_api;


void i2c_init(struct i2c *dev, void *ctx)
{
    dev->ctx = ctx;
    dev->mutex = xSemaphoreCreateMutex();
    dev->api = &stm32_i2c_api;
    dev->api->init(dev, ctx);
}

i2c_res_t i2c_read_reg(struct i2c *dev, uint8_t addr, uint8_t reg, uint8_t *data, int32_t len)
{
    i2c_res_t ret = I2C_ERROR;

    if (xSemaphoreTake(dev->mutex, 10) == pdTRUE) {
        ret = dev->api->read_reg(dev, addr, reg, data, len);
        xSemaphoreGive(dev->mutex);
    }
    return ret;
}

i2c_res_t i2c_write_reg(struct i2c *dev, uint8_t addr, uint8_t reg, uint8_t *data, int32_t len)
{
    i2c_res_t ret = I2C_ERROR;

    if (xSemaphoreTake(dev->mutex, 10) == pdTRUE) {
        ret = dev->api->write_reg(dev, addr, reg, data, len);
        xSemaphoreGive(dev->mutex);
    }
    return ret;
}
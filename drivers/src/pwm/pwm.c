#include "FreeRTOS.h"
#include "semphr.h"
#include "pwm/pwm.h"


void pwm_init(struct pwm *pwm_ch, pwm_ch_t ch, void *ctx)
{
    pwm_ch->mutex = xSemaphoreCreateMutex();
    pwm_ch->api = &stm32_pwm_api;
    pwm_ch->api->init(pwm_ch, ch, ctx);
}

void pwm_set_width_us(struct pwm *pwm_ch, uint32_t us)
{
    if (us > 2000) {
        us = 2000;
    } else if (us < 900) {
        us = 900;
    }
    if (xSemaphoreTake(pwm_ch->mutex, 10) == pdTRUE) {
        pwm_ch->api->set_width_us(pwm_ch, us);
        xSemaphoreGive(pwm_ch->mutex);
    }
}

uint32_t pwm_get_width_us(struct pwm *pwm_ch)
{
    uint32_t ret = 0;
    if (xSemaphoreTake(pwm_ch->mutex, 10) == pdTRUE) {
        ret = pwm_ch->api->get_width_us(pwm_ch);
        xSemaphoreGive(pwm_ch->mutex);
    }
    return ret;
}
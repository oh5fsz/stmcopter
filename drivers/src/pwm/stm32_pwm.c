#include <stm32f1xx.h>
#include "FreeRTOS.h"
#include "semphr.h"
#include "pwm/pwm.h"

#define TIM_PRESCALER (uint32_t)23
#define TIM_ARR (uint32_t)60000


static uint32_t stm32_pwm_us_to_cycles(uint32_t width_us)
{
    uint32_t ret;
    /* Constant defined by CPU clock frequency and prescaler */
    ret = (uint32_t)(width_us * 3);
    if (ret > 2000 * 3) {
        ret = 2000 * 3;
    } else if (ret < 900 * 3) {
        ret = 900 * 3;
    }
    return ret;
}

void stm32_pwm_init(struct pwm *pwm_ch, pwm_ch_t ch, void *ctx)
{
    pwm_ch->channel = ch;
    pwm_ch->width = 1000;

    /* Enable clock for TIM1 and GPIOA */
    RCC->APB2ENR |= RCC_APB2ENR_TIM1EN | RCC_APB2ENR_IOPAEN | RCC_APB2ENR_AFIOEN;
    /* Enable autoreload preload */
    TIM1->CR1 |= TIM_CR1_ARPE;
    /* Set prescaler */
    TIM1->PSC = TIM_PRESCALER;
    TIM1->ARR = TIM_ARR;
    /* Set to PWM mode 1 */
    TIM1->CCMR1 |= (6 << TIM_CCMR1_OC1M_Pos)
                | (6 << TIM_CCMR1_OC2M_Pos)
                | TIM_CCMR1_OC1FE 
                | TIM_CCMR1_OC2FE;
    TIM1->CCMR2 |= (6 << TIM_CCMR2_OC3M_Pos)
                | (6 << TIM_CCMR2_OC4M_Pos)
                | TIM_CCMR2_OC3FE
                | TIM_CCMR2_OC4FE;
    /* Enable capture/compare outputs */
    TIM1->CCER |= TIM_CCER_CC1E
                | TIM_CCER_CC2E
                | TIM_CCER_CC3E
                | TIM_CCER_CC4E;
    /* Set PA{8,9,10,11} as AF outputs */
    GPIOA->CRH &= ~((3 << GPIO_CRH_CNF8_Pos)
                | (3 << GPIO_CRH_CNF9_Pos)
                | (3 << GPIO_CRH_CNF10_Pos)
                | (3 << GPIO_CRH_CNF11_Pos)
                | (3 << GPIO_CRH_MODE8_Pos)
                | (3 << GPIO_CRH_MODE9_Pos)
                | (3 << GPIO_CRH_MODE10_Pos)
                | (3 << GPIO_CRH_MODE11_Pos));
    GPIOA->CRH |= (2 << GPIO_CRH_MODE8_Pos)
                | (2 << GPIO_CRH_MODE9_Pos)
                | (2 << GPIO_CRH_MODE10_Pos)
                | (2 << GPIO_CRH_MODE11_Pos)
                | (2 << GPIO_CRH_CNF8_Pos)
                | (2 << GPIO_CRH_CNF9_Pos)
                | (2 << GPIO_CRH_CNF10_Pos)
                | (2 << GPIO_CRH_CNF11_Pos);
    /* Enable counter and channels */
    TIM1->BDTR |= TIM_BDTR_MOE;
    TIM1->CR1 |= TIM_CR1_CEN;

    /*
        Enable output based on channel requested
        and set PWM period for arming the ESC
    */
    switch (ch) {
    case FRONT_LEFT:
        TIM1->CCR1 = stm32_pwm_us_to_cycles(990);
        TIM1->CCER |= TIM_CCER_CC1E;
        break;
    case FRONT_RIGHT:
        TIM1->CCR2 = stm32_pwm_us_to_cycles(990);
        TIM1->CCER |= TIM_CCER_CC2E;
        break;
    case REAR_LEFT:
        TIM1->CCR3 = stm32_pwm_us_to_cycles(990);
        TIM1->CCER |= TIM_CCER_CC3E;
        break;
    case REAR_RIGHT:
        TIM1->CCR4 = stm32_pwm_us_to_cycles(990);
        TIM1->CCER |= TIM_CCER_CC4E;
        break;
    }
    pwm_ch->width = 990;
}

void stm32_pwm_set_width_us(struct pwm *pwm_ch, uint32_t width_us)
{
    uint32_t cycles = stm32_pwm_us_to_cycles(width_us);

    switch (pwm_ch->channel) {
    case FRONT_LEFT:
        TIM1->CCR1 = cycles;
        break;
    case FRONT_RIGHT:
        TIM1->CCR2 = cycles;
        break;
    case REAR_LEFT:
        TIM1->CCR3 = cycles;
        break;
    case REAR_RIGHT:
        TIM1->CCR4 = cycles;
        break;
    }
    pwm_ch->width = width_us;
}

uint32_t stm32_pwm_get_width_us(struct pwm *pwm_ch)
{
    return pwm_ch->width;
}

struct pwm_api stm32_pwm_api = {
    .init = stm32_pwm_init,
    .set_width_us = stm32_pwm_set_width_us,
    .get_width_us = stm32_pwm_get_width_us
};

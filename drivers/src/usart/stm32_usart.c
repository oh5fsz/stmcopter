#include "stm32f1xx.h"
#include "usart/usart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "stream_buffer.h"


extern TaskHandle_t quad_cli_task_handle;
static struct usart *ports[3];

static struct usart *stm32_usart_to_port(USART_TypeDef *port)
{
    if (port == USART2) {
        return ports[0];
    }
    return NULL;
}

static void stm32_usart_init(struct usart *port, void *ctx)
{
    USART_TypeDef *p_usart = (USART_TypeDef*)ctx;

    if (p_usart == USART2) {
        ports[0] = port;
        /* Set PA2, PA3 as AFIO outputs (push-pull) */
        GPIOA->CRL &= ~((3 << GPIO_CRL_MODE2_Pos)
                    | (3 << GPIO_CRL_MODE3_Pos)
                    | (3 << GPIO_CRL_CNF2_Pos)
                    | (3 << GPIO_CRL_CNF3_Pos));
        GPIOA->CRL |= (3 << GPIO_CRL_MODE2_Pos)
                    | (2 << GPIO_CRL_CNF2_Pos)
                    | (2 << GPIO_CRL_CNF3_Pos);
        /* Enable clock to USART2 */
        RCC->APB1ENR |= RCC_APB1ENR_USART2EN;
        /* Set baudrate to 115200 */
        USART2->BRR = (19 << USART_BRR_DIV_Mantissa_Pos) | (8 << USART_BRR_DIV_Fraction_Pos);
        /* Enable USART2 */
        USART2->CR1 |= USART_CR1_RE | USART_CR1_TE | USART_CR1_UE | USART_CR1_RXNEIE;
        NVIC_SetPriority(USART2_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 11, 0));
        NVIC_EnableIRQ(USART2_IRQn);
    }
}

static int stm32_usart_read(struct usart *port, char *buf, int len)
{
    return xStreamBufferReceive(port->rxq, buf, len, portMAX_DELAY);
}

static int stm32_usart_write(struct usart *port, char *buf, int len)
{
    int i;
    USART_TypeDef *usart = (USART_TypeDef*)port->ctx;

    xStreamBufferSend(port->txq, buf, len, 100);
    usart->CR1 |= USART_CR1_TXEIE | USART_CR1_TCIE;
    xSemaphoreTake(port->_txsem, portMAX_DELAY);
    return len;
}

void USART2_IRQHandler()
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    uint8_t c;
    struct usart *port = stm32_usart_to_port(USART2);

    if (USART2->SR & USART_SR_RXNE) {
        c = USART2->DR;
        port->rx_count++;
        xStreamBufferSendFromISR(port->rxq, &c, 1, &xHigherPriorityTaskWoken);
    }

    if (USART2->CR1 & USART_CR1_TXEIE) {
        if (USART2->SR & USART_SR_TXE) {
            if (xStreamBufferReceiveFromISR(port->txq, &c, 1, &xHigherPriorityTaskWoken) == 1) {
                USART2->DR = c;
                port->tx_count++;
            } else {
                USART2->CR1 &= ~USART_CR1_TXEIE;
            }
        }
    }

    if (USART2->CR1 & USART_CR1_TCIE) {
        if (USART2->SR & USART_SR_TC) {
            xSemaphoreGiveFromISR(port->_txsem, &xHigherPriorityTaskWoken);
            USART2->CR1 &= ~USART_CR1_TCIE;
        }
    }
    portYIELD_FROM_ISR(&xHigherPriorityTaskWoken);
}

struct usart_api stm32_usart_api = {
    .init = stm32_usart_init,
    .read = stm32_usart_read,
    .write = stm32_usart_write
};

#include "FreeRTOS.h"
#include "stream_buffer.h"
#include "semphr.h"
#include "usart/usart.h"

extern struct usart_api stm32_usart_api;

void usart_init(struct usart *port, void *ctx)
{
    port->api = &stm32_usart_api;
    port->mutex = xSemaphoreCreateMutex();
    port->_txsem = xSemaphoreCreateBinary();
    port->rxq = xStreamBufferCreate(128, 1);
    port->txq = xStreamBufferCreate(128, 1);
    port->ctx = ctx;
    port->api->init(port, ctx);
}

int usart_read(struct usart *port, char *buf, int len)
{
    return port->api->read(port, buf, len);
}

int usart_write(struct usart *port, char *buf, int len)
{
    return port->api->write(port, buf, len);
}

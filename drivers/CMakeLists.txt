SET(DRIVER_SOURCES
    src/pwm/pwm.c
    src/pwm/stm32_pwm.c
    src/usart/stm32_usart.c
    src/usart/usart.c
    src/i2c/i2c.c
    src/i2c/stm32_i2c.c
    src/receiver/receiver.c
    src/receiver/stm32_ppm.c
)

add_definitions("-DSTM32F103x6")
add_library(stmcopter_drivers STATIC ${DRIVER_SOURCES})
target_include_directories(stmcopter_drivers PUBLIC inc ${CMSIS_INCLUDE_DIRS})
target_link_libraries(stmcopter_drivers printf)
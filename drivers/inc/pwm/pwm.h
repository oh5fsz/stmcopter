#ifndef _PWM_H
#define _PWM_H

#include <stdint.h>
#include "FreeRTOS.h"
#include "semphr.h"

struct pwm;
struct pwm_api;

extern struct pwm_api stm32_pwm_api;

typedef enum
{
    FRONT_LEFT,
    FRONT_RIGHT,
    REAR_LEFT,
    REAR_RIGHT
} pwm_ch_t;

struct pwm
{
    SemaphoreHandle_t mutex;
    pwm_ch_t channel;
    uint32_t width;

    struct pwm_api *api;
};

struct pwm_api
{
    void (*init)(struct pwm *, pwm_ch_t, void *);
    void (*set_width_us)(struct pwm *, uint32_t);
    uint32_t (*get_width_us)(struct pwm*);
};

void pwm_init(struct pwm *pwm_ch, pwm_ch_t ch, void *ctx);
void pwm_set_width_us(struct pwm *pwm_ch, uint32_t us);
uint32_t pwm_get_width_us(struct pwm *pwm_ch);


#endif /* _PWM_H */
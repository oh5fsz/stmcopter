#ifndef _USART_H
#define _USART_H

#include <stdint.h>
#include "FreeRTOS.h"
#include "semphr.h"
#include "stream_buffer.h"

struct usart;
struct usart_api;

struct usart
{
    SemaphoreHandle_t mutex;
    SemaphoreHandle_t _txsem;
    StreamBufferHandle_t rxq;
    StreamBufferHandle_t txq;
    volatile uint32_t rx_count;
    volatile uint32_t tx_count;

    void *ctx;
    struct usart_api *api;
};

struct usart_api
{
    void (*init)(struct usart*, void *);
    int (*read)(struct usart*, char *, int);
    int (*write)(struct usart*, char *, int);
};

void usart_init(struct usart *port, void *ctx);
int usart_read(struct usart *port, char *buf, int len);
int usart_write(struct usart *port, char *buf, int len);

#endif /* _USART_H */
#ifndef _PPM_H
#define _PPM_H

#include <stdint.h>
#include "FreeRTOS.h"
#include "semphr.h"

struct receiver;
struct receiver_api;

#define MAX_CHANNELS 6

#define ELEVATOR_CH 0
#define AILERON_CH 1
#define THROTTLE_CH 2
#define RUDDER_CH 3
#define SWITCH1_CH 4
#define SWITCH2_CH 5

struct receiver
{
    SemaphoreHandle_t mutex;
    uint32_t channels[MAX_CHANNELS];

    struct receiver_api *api;
};

struct receiver_api
{
    int (*init)(struct receiver *, void *);
};

int receiver_init(struct receiver *rcvr, void *ctx);
int receiver_get_channels(struct receiver *rcvr, uint32_t *val);

#endif /* _PPM_H */
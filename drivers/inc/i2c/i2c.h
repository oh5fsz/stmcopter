#ifndef _I2C_H
#define _I2C_H

#include <stdint.h>
#include "FreeRTOS.h"
#include "semphr.h"

struct i2c_api;
struct i2c;

typedef enum
{
    I2C_ERROR,
    I2C_OK
} i2c_res_t;

struct i2c
{
    SemaphoreHandle_t mutex;
    uint32_t rx_count;
    uint32_t tx_count;

    void *ctx;
    struct i2c_api *api;
};

struct i2c_api
{
    void (*init)(struct i2c *, void *);
    i2c_res_t (*read_reg)(struct i2c  *, uint8_t, uint8_t, uint8_t *, int32_t);
    i2c_res_t (*write_reg)(struct i2c  *, uint8_t, uint8_t, uint8_t *, int32_t);
};

void i2c_init(struct i2c *dev, void *ctx);
i2c_res_t i2c_read_reg(struct i2c *dev, uint8_t addr, uint8_t reg, uint8_t *data, int32_t len);
i2c_res_t i2c_write_reg(struct i2c *dev, uint8_t addr, uint8_t reg, uint8_t *data, int32_t len);

#endif /* _I2C_h */